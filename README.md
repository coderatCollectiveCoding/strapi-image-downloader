# Strapi Image Downloader
[Package on npmjs.com >>>](https://www.npmjs.com/package/strapi-image-downloader)

This package includes a script that downloads images from Strapi. It's particularly useful as a prebuild script for static JavaScript applications that use Strapi as a CMS. This allows you to store images as static assets, rather than loading them remotely.

* The script downloads all images that are used in any attribute of any Strapi record. It does not download images that are simply uploaded to the Media Library. Make sure, that the uploads GET endpoint of the Strapi API is set to public!
* All downloaded images are saved in their original version and in three resized versions (see the created uploads folder).

## Installation
```npm install strapi-image-downloader```

or

```yarn add strapi-image-downloader```

## Usage
```
saveMediaFromStrapi(strapiUrl, staticPath, [fetchAll], [widths])
```
Here, `strapiUrl` is the absolute URL of the Strapi instance (for example, http://localhost:1337) and `staticPath` is the output folder, which should be the `public` or `static` in your app. Note, that the script creates a subfolder `uploads`.

`fetchAll` is optional. If it is set true, not only images are loaded which have related items, but all images from the media library. This might be necessary in some Strapi-based apps, since images related to deeper items (e.g. repeatable components) can not easily be detected as being related via "related" field.

`widths` is optional. Set an array of three integers defining the resolution, images are resized to. Default is [300, 800, 1800]. If you set it, add the argument to the StrapiImage component, too (see below).

## Example
In a JavaScript application like SvelteKit or Next,create a `prebuild.mjs` file in the root directory with the following content:

``` js
import { saveMediaFromStrapi } from 'strapi-image-downloader';

saveMediaFromStrapi(process.env.PUBLIC_STRAPI_URL, 'public')
```

Note: 'public' is the default folder for static files in the app. Adjust it if necessary.

In your package.json, you can set the prebuild script to run prior to building:

```json
  "scripts": {
    "dev": "node prebuild.mjs && next dev",
    "build": "node prebuild.mjs && build"
```

Now, you can use the image URL provided by Strapi (for example, `post.data.attributes.headerImage.data.attributes.url`) as the relative path now matches your downloaded image path.

## Extra feature: Svelte and React components
In your Svelte (or SvelteKit) or React (only Next.js tested) app, you can use the built in StrapiImage component. For each image it uses the three downscaled versions processed by the `saveMediaFromStrapi` script in order to build a responsive image using image-srcset. If you have defined custom image widths, add them here, too: `<StrapiImage widths={[300, 800, 1800]} ...`

### Usage example SvelteKit
```html
<script>
  import { StrapiImage } from 'strapi-image-downloader/svelte';

  export let image;
</script>

<div class="hero-image">
  <StrapiImage class="w-100" src={image.attributes.url} alt="A red cat" />
</div>
```

### Usage example Next.js

```js
import { StrapiImage } from "strapi-image-downloader/react";

function MyComponent({ data }) {
  return (
    <div class="hero-image">
      <StrapiImage className="w-100" src={image.attributes.url} />
    </div>
  )
}
```

## Development
Run `npm link` from this repository and `npm link strapi-image-downloader`in the project you want to import this package from. After developing, unlink with `npm unlink strapi-image-downloader`

## Make releases
Increae version number in package.json and then run `npm publish`
