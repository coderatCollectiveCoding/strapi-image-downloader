import fs from "node-fs";
import Jimp from "jimp";
import fetch from "node-fetch";

// RESIZE IMAGE HELPER
async function resizeImage(file, resizings, imagePath) {
  const resizePromises = resizings.map(async (width) => {
    try {
      const image = await Jimp.read(imagePath);
      console.log(" resize file", file, "to", width, "px");
      await image
        .quality(85)
        .resize(width, Jimp.AUTO)
        .writeAsync(
          imagePath
            .replace("uploads/", `uploads/${width}_`)
            .replace(/\.\w+$/, ".jpg"),
        );
    } catch (err) {
      throw new Error(
        `Error resizing image ${file} to ${width}px: ${err.message}`,
      );
    }
  });

  return Promise.all(resizePromises);
}

// SEARCH FOR MISSING IMAGES
function searchMissingImages(uploads, staticPath, fetchAll, resizings) {
  let imagesToDownload = [];
  const savedFiles = fs.readdirSync(`${staticPath}/uploads`);
  const uploadedImages = uploads
    .filter(
      (upload) => fetchAll || (upload.related && upload.related.length > 0),
    )
    .map((upload) => upload.url.replace("/uploads/", ""));

  uploadedImages.forEach((file) => {
    // do not download if file is no image
    if (!file.match(/\.(png|jpg|jpeg|webp|svg|gif)$/i)) {
      return;
    }
    // do not download if file is already saved, otherwise add key to imagesToDownload
    if (!savedFiles.includes(file)) {
      imagesToDownload.push({ file, resizings: [] });
    }
    // ignore resizing if no resizable image
    if (!file.match(/\.(png|jpg|jpeg|webp)$/i)) {
      return;
    }
    // resize
    const fileJpg = file.replace(/\.\w+$/, ".jpg");
    resizings.forEach((width) => {
      if (!savedFiles.includes(`${width}_${fileJpg}`)) {
        const imageEntry = imagesToDownload.find((i) => i.file === file);
        if (imageEntry) {
          imageEntry.resizings.push(width);
        } else {
          imagesToDownload.push({ file, resizings: [width] });
        }
      }
    });
  });
  console.log("To be downloaded:", imagesToDownload);

  return imagesToDownload;
}

// DOWNLOAD AND RESIZE IMAGES
async function processImages(
  strapiUrl,
  uploads,
  staticPath,
  fetchAll,
  resizings,
) {
  const imagesToDownload = searchMissingImages(
    uploads,
    staticPath,
    fetchAll,
    resizings,
  );

  let counter = 0;
  const totalImages = imagesToDownload.length;

  const downloadPromises = imagesToDownload.map(async (image) => {
    const file = image["file"];
    const response = await fetch(`${strapiUrl}/uploads/${file}`);
    const buffer = await response.arrayBuffer();
    const imagePath = `${staticPath}/uploads/${file}`;

    console.log(" file", file);
    fs.writeFileSync(imagePath, Buffer.from(buffer));

    // Resize image if applicable
    if (image["resizings"].length > 0) {
      await resizeImage(file, image["resizings"], imagePath, resizings);
    }

    counter++;
    console.log(`${counter}/${totalImages} images downloaded`);
  });

  await Promise.all(downloadPromises);
  console.log("All images processed");
}

// MAIN FUNCTION
export async function saveMediaFromStrapi(
  strapiUrl,
  staticPath,
  fetchAll = false,
  resizings = [150, 600, 1600],
) {
  console.log("##### STRAPI-IMAGE-DOWNLOADER 2.0.2 #####");
  console.log("### ATTENTION: This version does convert every PNG to JPG! ###");

  if (!strapiUrl) {
    console.error(
      "ERROR: Argument 'strapiUrl' not declared in the function call!",
    );
    return;
  } else {
    console.log("Strapi URL:", strapiUrl);
  }

  if (!staticPath) {
    console.error(
      "ERROR: Argument 'staticPath' not declared in the function call!",
    );
    return;
  } else {
    console.log("Static path:", staticPath);
  }

  // create export folder if not exists
  if (!fs.existsSync(`${staticPath}/uploads`)) {
    console.log("Creating uploads folder in path", staticPath, "...");
    fs.mkdirSync(`${staticPath}/uploads`, { recursive: true });
  }

  console.log("Starting to download images...");
  try {
    const response = await fetch(`${strapiUrl}/api/upload/files?populate=*`);
    const data = await response.json();
    await processImages(strapiUrl, data, staticPath, fetchAll, resizings);
  } catch (error) {
    console.error("Error processing images:", error);
  }
}
