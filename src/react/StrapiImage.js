// lazysizes has to run on client side
// Maybe this works only in Next.js
"use client";

import React, { useEffect } from "react";
import "lazysizes";

const StrapiImage = ({
  src,
  alt,
  style,
  className,
  widths = [150, 600, 1600],
}) => {
  // lazysizes has to run on client side
  useEffect(() => {
    if (typeof window !== "undefined") {
      import("lazysizes");
    }
  }, []);

  const srcWidth = (width) => {
    return src.replace("uploads/", `uploads/${width}_`);
  };

  if (!src.match(/\.(svg|gif|webp)$/i)) {
    return (
      <img
        alt={alt != null ? alt : " "}
        className={`${className} lazyload`}
        style={style}
        // these sources are loaded by lazysizes
        data-src={srcWidth(widths[2])}
        data-sizes="auto"
        data-srcset={`
          ${srcWidth(widths[0])} ${widths[0]}w,
          ${srcWidth(widths[1])} ${widths[1]}w,
          ${srcWidth(widths[2])} ${widths[2]}w,
        `}
        // fallback image and placeholder before lazyload has loaded the bigger images
        src={srcWidth(widths[0])}
      />
    );
  } else {
    return (
      <img
        src={src}
        alt={alt != null ? alt : " "}
        className={className}
        style={style}
      />
    );
  }
};

export { StrapiImage };
