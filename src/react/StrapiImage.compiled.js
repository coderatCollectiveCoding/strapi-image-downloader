// lazysizes has to run on client side
// Maybe this works only in Next.js
"use client";

import React, { useEffect } from "react";
import "lazysizes";
var StrapiImage = function StrapiImage(_ref) {
  var src = _ref.src,
    alt = _ref.alt,
    style = _ref.style,
    className = _ref.className,
    _ref$widths = _ref.widths,
    widths = _ref$widths === void 0 ? [150, 600, 1600] : _ref$widths;
  // lazysizes has to run on client side
  useEffect(function () {
    if (typeof window !== "undefined") {
      import("lazysizes");
    }
  }, []);
  var srcWidth = function srcWidth(width) {
    return src.replace("uploads/", "uploads/".concat(width, "_"));
  };
  if (!src.match(/\.(svg|gif|webp)$/i)) {
    return /*#__PURE__*/ React.createElement("img", {
      alt: alt != null ? alt : " ",
      className: "".concat(className, " lazyload"),
      style: style,
      // these sources are loaded by lazysizes
      "data-src": srcWidth(widths[2]),
      "data-sizes": "auto",
      "data-srcset": "\n          "
        .concat(srcWidth(widths[0]), " ")
        .concat(widths[0], "w,\n          ")
        .concat(srcWidth(widths[1]), " ")
        .concat(widths[1], "w,\n          ")
        .concat(srcWidth(widths[2]), " ")
        .concat(widths[2], "w,\n        "),
      // fallback image and placeholder before lazyload has loaded the bigger images
      src: srcWidth(widths[0]),
    });
  } else {
    return /*#__PURE__*/ React.createElement("img", {
      src: src,
      alt: alt != null ? alt : " ",
      className: className,
      style: style,
    });
  }
};
export { StrapiImage };
